% -*- coding: utf-8 -*-
%
% Memoir-based template for articles v1.2.0 (2015-04-21)
%
% Yann Vote
%
% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International license: http://creativecommons.org/licenses/by-sa/4.0/
%
\documentclass[a4paper,twoside,11pt,onecolumn,article]{memoir}

% Update with your information (see also 'Choose your license' below)
\newcommand{\DocumentTitle}{Premier point d'étate du stage}
\newcommand{\AuthorFirstName}{Yann}
\newcommand{\AuthorLastName}{Voté}
\newcommand{\AuthorEmail}{yann.vote@gmail.com}
\newcommand{\Context}{Master SIGMA 2014/2015}
\newcommand{\Institution}{Université de Toulouse/Logilab}

% Load packages specific for pdf (so must use LuaLaTeX/XeLaTeX)
\usepackage{hyperref}
\usepackage{hyperxmp}

% Fonts, languages & symbols (this must be before custom)
\usepackage{fontspec} % Font selection (LuaLaTeX/XeLaTex specific)
\setmainfont{TeX Gyre Pagella}
\usepackage{microtype} % Slightly tweak font spacing for aesthetics
\usepackage{polyglossia} % Multi-language support
\setdefaultlanguage{french} % French by default

% My custom settings
\usepackage{custom}

% Choose your license
\usepackage{csquotes}
\usepackage[
	type={CC},
	modifier={by-sa},
	version={4.0},
]{doclicense}

% Allows abstract customization
\usepackage{abstract}
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

% Page layout
\setlrmarginsandblock{21mm}{31mm}{*} % Left & right margins
\setulmarginsandblock{26mm}{*}{*} % Upper margin
\setheadfoot{14pt}{34pt} % Header and footer size
\setheaderspaces{*}{20pt}{*} % Space between header and text
\checkandfixthelayout % Apply changes

% Headers and footers
\nouppercaseheads
\makeheadrule{headings}{\textwidth}{\normalrulethickness}
\makeevenhead{headings}{\normalfont\bfseries\thepage}{}{\normalfont\small\bfseries\rightmark}
\makeoddhead{headings}{\normalfont\small\bfseries\DocumentTitle}{}{\normalfont\bfseries\thepage}
\makeevenfoot{headings}{}{}{\normalfont\sffamily\scriptsize\Institution{} --- \Context}
\makeoddfoot{headings}{\normalfont\sffamily\scriptsize\textcopyright~\the\year~\href{mailto:\AuthorEmail}{\AuthorFirstName~\textsc{\AuthorLastName}}.~\doclicenseText}{}{}

% Title typesetting
\title{\vspace{-15mm}\fontsize{24pt}{10pt}\selectfont\textbf{\DocumentTitle}} % Article title
\author{%
	\large
	\noindent\textsc{\AuthorFirstName~\textsc{\AuthorLastName}}\\
	\noindent\normalsize \Institution{} --- \Context
	\vspace{-5mm}
}
\date{\today}

% Sectioning
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections

% Captions typesetting
\captiondelim{ -- }
\captiontitlefont{\itshape}

% Generate dummy text. Remove when creating real document
\usepackage{lipsum}


\begin{document}

\pagestyle{headings}
\maketitle


\section{Contexte}

\lettrine[nindent=0em,lines=2]{L} e stage a pour sujet «~\textit{Développement
en Python d'un outil de recherche et d'agrégation de données localisées
provenant de sources différentes}~».

Il s'agit de développer un portail web qui collecte des informations
géolocalisées provenant de multiples producteurs de données et de différents
catalogues géographiques et qui «~aligne~» ces informations, c'est-à-dire qu'il
trouve des liens entre elles de façon à présenter sur l'interface web des
données liées. Un exemple de tel portail est celui de la BNF:
\href{http://data.bnf.fr/}{http://data.bnf.fr/}.

Un aspect important à prendre en compte est donc celui du web sémantique et
l'outil devra s'appuyer au maximum sur les standards associés.

Ce stage se déroule au sein de l'agence Logilab de Toulouse, société de
services informatiques dont le siège se trouve à Paris. Logilab propose
essentiellement des services de développement à ses clients, en utilisant des
méthodologies de développement modernes, fondées sur les principes Agiles, afin
d'améliorer l'efficacité des équipes et de livrer des logiciels de qualité.
Logilab propose aussi des formations sur le langage Python et les outils qui
gravitent autour de ce langage.


\section{Vision du produit final et périmètre du stage}

Logilab est aussi la société qui développe la suite CubicWeb, un
\foreign{framework open source} de développement web. La spécificité de
CubicWeb par rapport à d'autres \foreign{frameworks}, qu'ils soient PHP ou
Python, est la grande facilité avec laquelle on peut réutiliser des
applications déjà développées avec CubicWeb, qu'on appelle des cubes. Il est
ainsi possible, avec quelques lignes de code, de combiner un cube gérant un
blog et un autre gérant un wiki pour en former un troisième plus complet
offrant ces deux fonctionnalités.

Le portail développé dans le stage s'appuiera donc sur ce \foreign{framework}.
\href{http://data.bnf.fr/}{http://data.bnf.fr/} est d'ailleurs propulsé par
CubicWeb. L'un des objectifs et de s'inspirer de ce qui a été fait dans ce cas
et d'ajouter un aspect géographique qui manque encore à CubicWeb.

Cela s'inscrit dans un projet plus vaste de développement d'un portail de
partage de données, tourné aussi bien vers le grand public que vers d'autres
partenaires et en interne, pouvant répondre aux besoins de plusieurs
collectivités. Cette vision a été présentée à l'ensemble de l'équipe après 15
jours d'étude bibliographique et d'état de l'art des solutions existantes
(figure~\ref{fig.solution-finale}).

\begin{figure}[H]
	\centering
	\includegraphics[max width=.9\textwidth]{solution.png}
	\caption{Vision d'un produit final}\label{fig.solution-finale}
\end{figure}

Néanmoins dans le temps imparti des 6 mois du stage, le développement se
limitera à traiter les points suivants:
\begin{compactitem}
\item ajout des types permettant de stocker des données géographiques dans
	CubicWeb avec Postgis;
\item ajout des vues permettant d'afficher et d'interagir avec ces données dans
	une page web (sans doute avec leaflet);
\item moissonnage (\foreign{i.e.} collecte des données) de différents types de
	sources (partie gauche de la figure);
\item alignement des données collectées;
\item création de services web REST permettant d'accéder aux données par une
	interface de programmation (en exportant sans doute les données en RDF et en
	WFS).
\end{compactitem}


\section{Planning prévisionnel}

Comme évoqué dans la section précédente, les 15 premiers jours du stage ont été
dédiés à une étude très générale sur les besoins identifiables, les outils
existants qui y répondent plus ou moins complètement, leur forces et leurs
faiblesses, ainsi que sur les standards de l'OGC et du W3C à prendre en compte
dans le cadre de ce développement. Cette étude a débouché sur un diaporama
devant l'ensemble de l'équipe afin de présenter le projet et de discuter les
limites du stage (figure~\ref{fig.solution-finale}).

Quant au développement, il suit un certain nombres de principes agiles.
Chacune des fonctionnalités globales est découpée en tâches, et chaque tâche
fait l'objet d'une itération qui dure de 2 à 3 semaines. Le code est
systématiquement relu par un développeur plus expérimenté avant d'être intégré.
Le plus souvent les tests sont écrits avant le code lui-même conformément aux
règles du \foreign{Test Driven Development}. Le développement lui-même est
conduit de façon incrémentale, en commençant par écrire le minimum de code pour
avoir un produit qui fonctionne\footnote{Si le client demande un autobus,
commencer par lui montrer une bicyclette.}.

L'idée est donc de commencer par écrire un portail minimaliste, mais qui
fonctionne de bout en bout: d'une source de données jusqu'à l'affichage dans
CubicWeb de quelques données provenant de cette source, même si dans un premier
temps ces données sont minimalistes: un titre, une date et une géométrie. Dans
un second temps on s'attachera à récupérer l'ensemble des données disponibles,
à améliorer l'affichage, puis à réaliser des alignements entre ces données, et
enfin à développer des services web pour servir ces informations via une API
HTTP (table~\ref{tab.planning-prev}).

\begin{table}[H]
	\centering
	\begin{tabular}{lc}
		\toprule
		\multicolumn{1}{c}{\bfseries Itération} & \multicolumn{1}{c}{\bfseries Période}\\
		\midrule
		Moissonnage d'une source & 16--27~mars\\
		Moissonnage de la source + données géographiques &  30~mars--10~avril\\
		Affichage des données géographiques & 13--30~avril\\
		Moissonnage complet de la source & 4--13~mai\\
		Moissonnage complet d'autres sources & 18~mai--5~juin\\
		Interface web finale (ensemble des données) & 8--19~juin\\
		Alignement des données & 22~juin--10~juillet\\
		Services web et API HTTP & 13--31~juillet\\
		Cas des rasters & 3--28~août\\
		\bottomrule
	\end{tabular}
	\caption{Planning prévisionnel des itérations}\label{tab.planning-prev}
\end{table}

Aujourd'hui, j'ai pris un peu de retard car CubicWeb est un outil complexe et
cela prend du temps de l'apprivoiser, même avec l'aide des encadrants (et à
vrai dire de l'ensemble de la société grâce à un système de messagerie
instantanée permettant de discuter en direct avec l'équipe de Paris). J'en suis
toujours à moissonner des données géographiques, c'est-à-dire à rajouter la
capacité à CubicWeb de stocker des données géométriques dans une base Postgis.

Comme le montre aussi le tableau, le dernier mois du stage sera consacré à
étendre le portail afin de gérer des données rasters: on l'aura compris, le
stage se focalise surtout sur des données vectorielles.

\end{document}
