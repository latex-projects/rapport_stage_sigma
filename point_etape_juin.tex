% -*- coding: utf-8 -*-
%
% Memoir-based template for articles v1.2.0 (2015-04-21)
%
% Yann Vote
%
% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International license: http://creativecommons.org/licenses/by-sa/4.0/
%
\documentclass[a4paper,twoside,11pt,onecolumn,article]{memoir}

% Update with your information (see also 'Choose your license' below)
\newcommand{\DocumentTitle}{Second point d'étate du stage}
\newcommand{\AuthorFirstName}{Yann}
\newcommand{\AuthorLastName}{Voté}
\newcommand{\AuthorEmail}{yann.vote@gmail.com}
\newcommand{\Context}{Master SIGMA 2014/2015}
\newcommand{\Institution}{Université de Toulouse/Logilab}

% Load packages specific for pdf (so must use LuaLaTeX/XeLaTeX)
\usepackage{hyperref}
\usepackage{hyperxmp}

% Fonts, languages & symbols (this must be before custom)
\usepackage{fontspec} % Font selection (LuaLaTeX/XeLaTex specific)
\setmainfont{TeX Gyre Pagella}
\usepackage{microtype} % Slightly tweak font spacing for aesthetics
\usepackage{polyglossia} % Multi-language support
\setdefaultlanguage{french} % French by default

% My custom settings
\usepackage{custom}

% Choose your license
\usepackage{csquotes}
\usepackage[
	type={CC},
	modifier={by-sa},
	version={4.0},
]{doclicense}

% Allows abstract customization
\usepackage{abstract}
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

% Page layout
\setlrmarginsandblock{21mm}{31mm}{*} % Left & right margins
\setulmarginsandblock{26mm}{*}{*} % Upper margin
\setheadfoot{14pt}{34pt} % Header and footer size
\setheaderspaces{*}{20pt}{*} % Space between header and text
\checkandfixthelayout % Apply changes

% Headers and footers
\nouppercaseheads
\makeheadrule{headings}{\textwidth}{\normalrulethickness}
\makeevenhead{headings}{\normalfont\bfseries\thepage}{}{\normalfont\small\bfseries\rightmark}
\makeoddhead{headings}{\normalfont\small\bfseries\DocumentTitle}{}{\normalfont\bfseries\thepage}
\makeevenfoot{headings}{}{}{\normalfont\sffamily\scriptsize\Institution{} --- \Context}
\makeoddfoot{headings}{\normalfont\sffamily\scriptsize\textcopyright~\the\year~\href{mailto:\AuthorEmail}{\AuthorFirstName~\textsc{\AuthorLastName}}.~\doclicenseText}{}{}

% Title typesetting
\title{\vspace{-15mm}\fontsize{24pt}{10pt}\selectfont\textbf{\DocumentTitle}} % Article title
\author{%
	\large
	\noindent\textsc{\AuthorFirstName~\textsc{\AuthorLastName}}\\
	\noindent\normalsize \Institution{} --- \Context
	\vspace{-5mm}
}
\date{\today}

% Sectioning
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections

% Captions typesetting
\captiondelim{ -- }
\captiontitlefont{\itshape}

% Generate dummy text. Remove when creating real document
\usepackage{lipsum}


\begin{document}

\pagestyle{headings}
\maketitle


\section{Objectifs et périmètre}

\subsection{Formulation de l'objectif}

\lettrine[nindent=0em,lines=2]{L} e stage a pour sujet «~\textit{Développement
en Python d'un catalogue de données géo-localisées aggrégant des données
provenant de sources différentes}~».

L'objectif initial est donc de développer un portail web qui collecte des
informations géolocalisées provenant de différents catalogues géographiques et
qui «~aligne~» ces informations, c'est-à-dire qu'il trouve des liens entre
elles de façon à présenter sur l'interface web des données liées. Un exemple de
tel portail est celui de la BNF:
\href{http://data.bnf.fr/}{http://data.bnf.fr/}.

\subsection{Vision d'un produit final}

Ces développements s'inscrivent dans un projet plus vaste de portail de
partage de données géo-localisées:
\begin{compactitem}
\item vers le grand public (dans un esprit \foreign{open data});
\item vers des organisations partenaires ou clientes;
\item vers les collaborateurs en interne.
\end{compactitem}

Le premier cas est par exemple celui d'un organisme public soumis à des
règlementations l'obligeant à publier certaines de ses données, telles que la
directive Inspire de l'Union Européenne.

Le second cas pourrait être celui d'un bureau d'études fournissant les
résultats d'une étude à un client: il s'agirait de fournir un moyen simple,
ergonomique et adapté aux données géo-localisées de partager ces données par le
biais d'une interface web plutôt que de joindre des \foreign{Shapefiles} à des
courriers électroniques par exemple.

Le troisième cas est celui de n'importe quelle organisation travaillant avec
des données géo-localisées. Le produit serait un catalogue interne, indexant à
la fois les données produites par l'organisation, mais aussi des données
externes (référentiels de l'IGN par exemple). Le logiciel SIG utilisé au sein
de l'organisation (ArcGis ou QGIS) se connecterait, grâce à un protocole de
type CSW par exemple, au catalogue pour trouver et télécharger directement des
données provenant de sources différentes.

Cette vision a été présentée à l'ensemble de l'équipe après 15 jours d'étude
bibliographique, de recensement des besoins, et d'état de l'art des solutions
existantes (figure~\ref{fig.solution-finale}).

\begin{figure}[H]
	\centering
	\includegraphics[max width=.9\textwidth]{solution.png}
	\caption{Vision d'un produit final}\label{fig.solution-finale}
\end{figure}

Néanmoins dans le temps imparti des 6 mois du stage, le développement se
limitera à traiter les points suivants:
\begin{compactitem}
\item ajout des types permettant de stocker des données géographiques dans
	CubicWeb avec Postgis;
\item ajout des vues permettant d'afficher et d'interagir avec ces données dans
	une page web (sans doute avec leaflet);
\item moissonnage (\foreign{i.e.} collecte des données) de différents types de
	sources (partie gauche de la figure);
\item alignement des données collectées;
\item création de services web REST permettant d'accéder aux données par une
	interface de programmation (en exportant sans doute les données en RDF et en
	WFS).
\end{compactitem}


\section{Logilab et CubicWeb}

Ce stage se déroule au sein de l'agence Logilab de Toulouse, société de
services informatiques dont le siège se trouve à Paris. Logilab propose
essentiellement des services de développement à ses clients, en utilisant des
méthodologies de développement modernes, fondées sur les principes Agiles, afin
d'améliorer l'efficacité des équipes et de livrer des logiciels de qualité.
Logilab propose aussi des formations sur le langage Python et les outils
scientifiaues qui gravitent autour de ce langage (NumPy, SciPy, Pandas, etc.).

Logilab est aussi la société qui développe la suite CubicWeb, un
\foreign{framework open source} de développement web. La spécificité de
CubicWeb par rapport à d'autres \foreign{frameworks}, qu'ils soient écrits en
PHP ou en Python, est la grande facilité avec laquelle on peut réutiliser des
applications déjà développées avec CubicWeb, qu'on appelle des cubes. Il est
ainsi possible, en quelques lignes de code, de combiner un cube gérant un
blog et un autre gérant un wiki pour en former un troisième plus complet
offrant ces deux fonctionnalités.

Le portail développé dans le stage s'appuiera donc sur ce \foreign{framework}.
\href{http://data.bnf.fr/}{http://data.bnf.fr/} est d'ailleurs propulsé par
CubicWeb. L'un des objectifs et de s'inspirer de ce qui a été fait dans ce cas
et d'ajouter un aspect géographique qui manque encore à CubicWeb.


\section{Terminologie}


\subsection{Métadonnées et catalogues}

Dans les systèmes d'information des organisation en général, et en particulier
dans les systèmes d'information géographiques (SIG), les objest de base qui
sont manipulés sont les données.  Ces données peuvent être structurées (le plus
souvent en tableau dans des feuilles de calculs, fichiers CSV, ou bases de
données, mais de plus en plus dans des bases NoSQL avec la montée en puissance
du \foreign{Big Data}) ou non structurées (documents PDF, courriers
électroniques, images, vidéos, etc.).

Un point crucial pour une organisation afin qu'elle s'y retrouve dans la masse
des données qu'elle produit est de les indéxer, c'est-à-dire de renseigner de
façon correcte et pertinente les \definition{méta-données} sur ces données
(titre, date de saisie, etc.). Car même s'il existe des outils capable de
parcourir l'ensemble des données pour y trouver une information, ils sont de
moins en moins performants à mesure que le volume de données augmente, et le
coût informatique pour parcourir des données binaires comme des images ou des
vidéos (pour savoir si une personne y est présente par exemple) est énorme. Ce
n'est donc sans doute pas la bonne direction.

Renseigner les métadonnées est donc plus pertinent sur le long
terme\footnote{Mais renseigner les métadonnées manuellement est sans doute une
opération fastidieuse. L'indéxation automatique (particulièrement des données
binaires) est donc une voie d'avenir, mais ce n'est pas l'objet du stage.}. Or
les métadonnées étant elles aussi des données, il faut bien les stocker: c'est
le rôle d'un catalogue que de les gérer. Un \definition{catalogue} est une
application permettant de stocker les méta-données, de les présenter de façon
appropriée (fonction du métier), d'effectuer des recherches, et éventuellement
d'autres opérations encore.

\subsection{Moissonnage}

Le \definition{moissonnage} est l'action qui consiste à télécharger des données
depuis une source externe accessible sur le web et à importer ces données dans
une application interne, le plus souvent bien sûr après avoir effectué des
opérations de filtrage, de nettoyage et de transformation sur les données
d'origine.

Dans le cadre de ce stage, l'objectif est de moissonner des catalogues de
données géo-localisées déja disponibles pour aggréger les différentes données
dans un seul catalogue; on pourrait parler de «méta-catalogue», d'un catalogue
de catalogues.


\section{Point d'avancement}


Comme évoqué dans la section précédente, les 15 premiers jours du stage ont été
dédiés à une étude très générale sur les besoins identifiables, les outils
existants qui y répondent plus ou moins complètement, leur forces et leurs
faiblesses, ainsi que sur les standards de l'OGC et du W3C à prendre en compte
dans le cadre de ces développements. Cette étude a débouché sur un diaporama
devant l'ensemble de l'équipe afin de présenter le projet et de discuter des
limites du stage (figure~\ref{fig.solution-finale}).

Quant aux développements, il suivent un certain nombres de principes agiles.
Chacune des fonctionnalités globales est découpée en tâches, et chaque tâche
fait l'objet d'une itération qui dure de 2 à 3 semaines. Le code est
systématiquement relu par un développeur plus expérimenté avant d'être intégré
au produit final. Le plus souvent les tests sont écrits avant le code lui-même,
conformément aux règles du \foreign{Test Driven Development}. Le code est alors
écrit de façon incrémentale, en commençant par en écrire le minimum pour avoir
un produit qui fonctionne\footnote{Si le client demande un autobus, commencer
par lui montrer une bicyclette.}, puis en nettoyant et en factorisant afin
d'avoir du code de qualité.

L'idée est donc de commencer par écrire un portail minimaliste, mais qui
fonctionne de bout en bout: d'une source de données jusqu'à l'affichage dans
CubicWeb de quelques données provenant de cette source, même si dans un premier
temps ces données sont minimalistes: un titre, une date et une géométrie. Dans
un second temps on s'attachera à récupérer l'ensemble des données disponibles,
et à améliorer l'affichage.

\subsection{Fonctionnalités acquises}

À mi-stage, les itérations listées dans le tableau~\ref{tab.planning-done} sont
complètes, validées et intégrées au produit final.

\begin{table}[H]
	\centering
	\begin{tabular}{lc}
		\toprule
		\multicolumn{1}{c}{\bfseries Itération} & \multicolumn{1}{c}{\bfseries Période}\\
		\midrule
		Premier moissonnage de la source \url{data.gouv.fr} & 16--27~mars\\
		Moissonnage des données géographiques &  30~mars--17~avril\\
		Affichage des données géographiques & 20--7~mai\\
		Moissonnage d'une autre source: \url{open-data.euopa.eu} & 11~mai--12~juin\\
		Moissonnage de CKAN  \url{ckan.org} & 14~mai--26~juin\\
		\bottomrule
	\end{tabular}
	\caption{Itérations réalisées}\label{tab.planning-done}
\end{table}

Autrement dit, aujourd'hui le catalogue est capable de télécharger et
d'importer les données provenant de trois sources:
\begin{compactitem}
\item \url{data.gouv.fr};
\item \url{open-data.europa.eu};
\item n'importe quel catalogue utilisant CKAN (\url{ckan.org}).
\end{compactitem}

Une difficulté a consisté à trouver un modèle de donnée commun à l'ensemble des
sources moissonnées, dont les données ne sont pas organisées de la même façon.
Le modèle retenu tourne autour de trois entités:
\begin{compactitem}
\item \definition{jeu de données};
\item \definition{distribution}, qui est une version téléchargeable d'un jeu de données;
\item \definition{lieu} qui est un lieu géographique concerné par un jeu de données.
\end{compactitem}

Il y a une relation 1-N entre jeu de données et distribution (un jeu de données
peut avoir plusieurs distributions) et entre jeu de données et lieu (un jeu de
données peut concerner plusieurs lieux). Par exemple l'ensemble des arrêts de
bus de Toulouse métropole avec leurs coordoonées et leur nom sont des données
qui constituent un jeu de données. Ce jeu de données possède des métadonnées:
un titre («arrêts de bus de Toulouse Métropole»), une date (les arrêts au
15/01/2015), etc. Ce jeu de données peut être associé à plusieurs
distributions: un fichier CSV, un fichier Shapefile, etc. qui ont eux aussi des
méta-données (ex. taille du fichier en octets). Enfin ce jeu de données est
associé à plusieurs lieux: chaque commune de la métropole.

Il faut noter que pour stocker les données géométriques, il a fallu choisir un
type très permissif: \lstinline{GEOMETRYCOLLECTION} (au lieu de
\lstinline{MULTIPOLYGON}) car le portail européen désigne les pays par des
points\ldots

La figure~\ref{fig.dataset} présente une capture d'écran de la page du
catalogue concernant un jeu de données importé depuis \url{data.gouv.fr}. On y
voit les informations sur le jeu de données, le lieu concerné dans une carte,
et les distributions téléchargeables dans un cartouche à droite. La
figure~\ref{fig.dataset1} est une capture d'écran concernant une jeu de données
en provenance de \url{open-data.europa.eu}.

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{dataset.png}
	\caption{Exemple de jeu de données moissonné depuis \url{data.gouv.fr}}\label{fig.dataset}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[max width=\textwidth]{dataset1.png}
	\caption{Exemple de jeu de données moissonné depuis \url{open-data.europa.eu}}\label{fig.dataset1}
\end{figure}


Cependant les plus grosses et les plus nombreuses difficultés sont apparues
lors de l'implémentation du moissonnage de ce portail européen car les données
sont extrêmement mal formées et donc très compliquées à analyser. Par exemple
de nombreux champs sont mal renseignés (ex.  lien de téléchargement incorrect),
certains champs ont plusieurs valeurs, les lieux sont renseignés pour chaque
jeu de données ce qui conduit à des doublons, les dates sont parfois
renseignées comme du texte, etc. D'où la durée de cette itération qui a fait
prendre un retard considérable. Mais d'un autre côté, c'est formateur de
travailler avec des données aussi mal formées.

Suite à ces itérations, une première version du catalogue devrait être publiée
début juillet, et il sera possible de l'installer sur un serveur à des fins de
démonstration.


\subsection{Planning prévisionnel}

Pour le reste du stage, compte tenu:
\begin{compactitem}
\item du retard pris à cause du moissonnage du portail europpéen;
\item d'une demande d'un client potentiel d'être compatible Inspire,
\end{compactitem}
la partie alignement est abandonnée pour le stage. Elle est remplacée par la
mise en conformité du catalogue au format de données préconisé par la directive
Inspire.

De la même façon, l'implémentation de services web et d'une API HTTP pourra ne
pas se faire, au profit d'une intégration avec le logiciel GeoNetwork qui
propose déjà ce genre de services.

Le planning prévisionnel pour la dernière partie du stage est donc désormais
celui présenté dans le tableau~\ref{tab.planning-prev}.

\begin{table}[H]
	\centering
	\begin{tabular}{lc}
		\toprule
		\multicolumn{1}{c}{\bfseries Itération} & \multicolumn{1}{c}{\bfseries Période}\\
		\midrule
		Interface web améliorée & 29~juin--9~juillet\\
		Implémation du format Inspire & 11~juillet--30~juillet\\
		Services web et API HTTP ou intégration avec GeoNetwork & 2--13~août\\
		Cas des rasters & 16--27~août\\
		\bottomrule
	\end{tabular}
	\caption{Planning prévisionnel des itérations}\label{tab.planning-prev}
\end{table}

Les délais se resserent donc et certaines fonctionnalités, moins prioritaires,
sont mises de côté et laissées à des développements ultérieurs

\end{document}
